#include <iostream>
/*
 El programa recibira ocho numeros los cuales representaran dos pares de coordenadas y dos pares de alto ancho.
 La interseccion de los rectangulos sera devuelta en forma de arreglo.
*/
using namespace std;

int* nuevoRectangulo (int *, int *);
int main()
{
    int coordx1, coordy1, tamAlto1, tamAncho1, coordx2, coordy2, tamAlto2, tamAncho2, rectanguloA[4], rectanguloB[4], posicion = 0;
    cout << "Ingrese el valor de la coordenada x para el rectangulo A:\n";
    cin >> coordx1;
    rectanguloA[posicion] = {coordx1};
    posicion ++;  //Esta variable aumentara para guardar el valor ingresado en lugares distintos de la memoria
    cout << "Ingrese el valor de la coordenada y para el rectangulo A:\n";
    cin >> coordy1;
    rectanguloA[posicion] = {coordy1};
    posicion ++;
    cout << "Ingrese el valor del alto del rectangulo A:\n";
    cin >> tamAlto1;
    rectanguloA[posicion] = {tamAlto1};
    posicion ++;
    cout << "Ingrese el valor del ancho del rectangulo A:\n";
    cin >> tamAncho1;
    rectanguloA[posicion] = {tamAncho1};
    posicion = 0;  //La devulvo a cero puesto que empezaremos a ingresar datos a un nuevo arreglo
    cout << "Ingrese el valor de la coordenada x para el rectangulo B:\n";
    cin >> coordx2;
    rectanguloB[posicion] = {coordx2};
    posicion ++;
    cout << "Ingrese el valor de la coordenada y para el rectangulo B:\n";
    cin >> coordy2;
    rectanguloB[posicion] = {coordy2};
    posicion ++;
    cout << "Ingrese el valor del alto del rectangulo B:\n";
    cin >> tamAlto2;
    rectanguloB[posicion] = {tamAlto2};
    posicion ++;
    cout << "Ingrese el valor del ancho del rectangulo B:\n";
    cin >> tamAncho2;
    rectanguloB[posicion] = {tamAncho2};
    int *a_puntero = rectanguloA;
    int *b_puntero = rectanguloB;
    int *c_puntero = nuevoRectangulo(a_puntero, b_puntero);
    for (int i=0; i<4; i++)
    {
        if (i == 0)
            cout << "{" << *(c_puntero) << ", ";
        else if (i == 3)
            cout << *(c_puntero+i) << "}" << " son las coordenadas del rectangulo resultante." << endl;
        else
            cout << *(c_puntero+i) << ", ";  //Imprimo las coordenadas del nuevo rectangulo
    }
    return 0;
}

int* nuevoRectangulo (int *a, int *b)
{
    int *c = new int [4], posicionOriginalX, posicionOriginalY;
    if (*(a) == 0 && *(a+1) == 0)  //Compruebo si el rectangulo parte de la posicion (0,0)
    {
        *c = *(b);  //El nuevo rectangulo iniciara en donde comience el segundo rectangulo tanto en X como en Y
        *(c+1) = *(b+1);
        *(c+2) = *(a+2) - *(b);  //El ancho del nuevo rectangulo lo determinara el ancho del primer rectangulo menos la posicion inicial en X del segundo rectangulo
        *(c+3) = *(a+3) - *(b+1);
    }
    else if ((*(b) && *(b+1)) == 0) //Si es el rectangulo B el que inicia en (0,0) A toma el lugar de B y visceversa
    {
        *c = *(a);
        *(c+1) = *(a+1);
        *(c+2) = *(b+2) - *(a);
        *(c+3) = *(b+3) - *(a+1);
    }
    else if (*(a) < *(b) && *(a) != 0)  //Si las coordenadas no inician en (0,0) debemos reubicarlas dependiendo cual triangulo es el mas cercano al origen
    {
        posicionOriginalX = *(a);  //Guardo el valor original de las coordenadas en X y en Y
        posicionOriginalY = *(a+1);
        *(a) = 0; //Envio las coordenadas al origen
        *(a+1) = 0;
        *(b) -= posicionOriginalX;  //Para no alterar al rectangulo resultante resto a las coordenadas triangulo B lo que al rectangulo A
        *(b+1) -= posicionOriginalY;
        *c = *(b);    //De aqui en adelante tomo los rectangulos como en la condicion de arriba cuando A inicia en (0,0)
        *(c+1) = *(b+1);
        *(c+2) = *(a+2) - *(b);
        *(c+3) = *(a+3) - *(b+1);
        *(c) += posicionOriginalX;  //Como la posicion fue alterada debemos agregar lo que fue quitado al principio y volvera a su posicion inicial
        *(c+1) += posicionOriginalY;
    }
    else if (*(b) < *(a) && *(b) != 0)  //Si las coordenadas no inician en (0,0) debemos reubicarlas dependiendo cual triangulo es el mas cercano al origen
    {
        posicionOriginalX = *(b);  //Guardo el valor original de las coordenadas en X y en Y
        posicionOriginalY = *(b+1);
        *(b) = 0; //Envio las coordenadas al origen
        *(b+1) = 0;
        *(a) -= posicionOriginalX;  //Para no alterar al rectangulo resultante resto a las coordenadas triangulo B lo que al rectangulo A
        *(a+1) -= posicionOriginalY;
        *c = *(a);    //De aqui en adelante tomo los rectangulos como en la condicion de arriba cuando A inicia en (0,0)
        *(c+1) = *(a+1);
        *(c+2) = *(b+2) - *(a);
        *(c+3) = *(b+3) - *(a+1);
        *(c) += posicionOriginalX;  //Como la posicion fue alterada debemos agregar lo que fue quitado al principio y volvera a su posicion inicial
        *(c+1) += posicionOriginalY;
    }

    return c;
}
